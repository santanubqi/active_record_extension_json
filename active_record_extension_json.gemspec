Gem::Specification.new do |s|
  s.name        = 'active_record_extension_json'
  s.version     = '0.0.0'
  s.date        = '2018-01-04'
  s.summary     = "Fetch JSON array!"
  s.description = "This is extension written for PostgreSQL to directly fetch JSON array, and record"
  s.authors     = ["Santanu Karmakar", "Santanu Bhattachrya"]
  s.email       = 'kolkata@quantuminventions.com'
  s.files       = ["lib/active_record_extension_json.rb"]
end
