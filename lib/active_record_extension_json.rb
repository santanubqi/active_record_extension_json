require "active_support/concern"

module ActiveRecordExtension
  extend ActiveSupport::Concern

  class_methods do
    def find_json_array_by_sql(sql)
      psql = "select array_to_json(array_agg(row_to_json(t100))) AS json_data from(#{sanitize_sql(sql)}) t100"
      data = connection.execute(psql)[0]['json_data'] || '[]'
      api_response(data)
    end

    def find_json_array(*fields)
      psql = "select array_to_json(array_agg(row_to_json(t100))) AS json_data from(#{json_query(fields)}) t100"
      data = connection.execute(psql)[0]['json_data'] || '[]'
      api_response(data)
    end

    def find_json_record(id, *fields)
      psql = "select row_to_json(t) from(#{json_query(fields, id)}) t"
      connection.execute(psql)[0]['row_to_json']
    rescue IndexError => e
      nil
    end

    protected

    def api_response(data_array)
      parsed_response = JSON.parse(data_array)

      {
        data: parsed_response.map {|hash| hash.deep_transform_keys! { |key| key.camelize(:lower) }}
      }
    end

    def json_query(fields, id = nil)
      sql = if fields.blank?
              select("#{table_name}.*")
            else
              select("#{fields.collect(&:to_s).join(', ')}")
            end

      sql = sql.where(id: id).limit(1) if id
      sanitize_sql(sql.all.to_sql)
    end
  end
end

ActiveRecord::Base.send(:include, ActiveRecordExtension)
